﻿using NetcoreBase.Domain.Contracts;
using NetcoreBase.Domain.Contracts.Services;
using NetcoreBase.Domain.Entities;

namespace NetcoreBase.Domain.Implements
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Users>> GetAllUsers()
        {
            return await _unitOfWork.GetRepository<Users>().GetListAsync();
        }
    }
}
