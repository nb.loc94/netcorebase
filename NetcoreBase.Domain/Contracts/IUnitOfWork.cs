﻿using NetcoreBase.Domain.Contracts.Repositories;
using NetcoreBase.Shared.Data.Contracts;

namespace NetcoreBase.Domain.Contracts
{
    public interface IUnitOfWork: Shared.Data.Contracts.IUnitOfWork
    {
        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
        IUserRepository UserRepository { get; }
    }
}
