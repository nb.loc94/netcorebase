﻿using NetcoreBase.Domain.Entities;

namespace NetcoreBase.Domain.Contracts.Services
{
    public interface IUserService
    {
        public Task<List<Users>> GetAllUsers();
    }
}
