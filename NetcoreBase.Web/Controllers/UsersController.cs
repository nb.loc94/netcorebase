﻿using Microsoft.AspNetCore.Mvc;
using NetcoreBase.Domain.Contracts.Services;
using NetcoreBase.Domain.Entities;
using NetcoreBase.Shared.Models;

namespace NetcoreBase.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IUserService _userService;

        public UsersController(IUserService userService, ILogger<UsersController> logger)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet]
        [Route("getall")]
        [ProducesDefaultResponseType(typeof(BaseRespond<dynamic>))]
        public async Task<IActionResult> GetAllUser()
        {
            var result = await _userService.GetAllUsers();
            return Ok(new BaseRespond<List<Users>>
            {
                Status = StatusCodes.Status200OK,
                Message = "Success",
                Data = result
            });
        }
    }
}
