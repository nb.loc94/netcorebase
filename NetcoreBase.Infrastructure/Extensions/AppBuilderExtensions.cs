﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NetcoreBase.Domain.Contracts;

namespace NetcoreBase.Infrastructure.Extensions
{
    public static class AppBuilderExtensions
    {
        public static void AddDatabase(this IServiceCollection services, string? connection)
        {
            services.AddDbContext<NetcoreBaseContext>(options =>
            {
                options.UseSqlServer(connection, b => b.UseRelationalNulls(true));
                options.EnableSensitiveDataLogging();
            });
        }

        public static void AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
