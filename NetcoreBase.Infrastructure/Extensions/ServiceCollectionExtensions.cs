﻿using Microsoft.Extensions.DependencyInjection;
using NetcoreBase.Domain.Contracts.Services;
using NetcoreBase.Domain.Implements;

namespace NetcoreBase.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {
            _ = services
                .AddTransient<IUserService, UserService>();
        }
    }
}
