﻿using Microsoft.AspNetCore.DataProtection.Repositories;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using NetcoreBase.Domain.Contracts;
using NetcoreBase.Domain.Contracts.Repositories;
using NetcoreBase.Shared.Data.Contracts;
using NetcoreBase.Shared.Data.Implements;
using IUnitOfWork = NetcoreBase.Domain.Contracts.IUnitOfWork;

namespace NetcoreBase.Infrastructure
{
    public sealed class UnitOfWork : Shared.Data.Implements.UnitOfWork, IUnitOfWork
    {
        private readonly NetcoreBaseContext _context;
        private readonly Dictionary<Type, object> _repositories;
        public IUserRepository UserRepository { get; private set; }
        public UnitOfWork(NetcoreBaseContext context) : base(context)
        {
            _context = context;
            _repositories = new Dictionary<Type, object>();
        }
        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            Type genericRepositoryInterfaceType = typeof(IGenericRepository<TEntity>);
            if (_repositories.TryGetValue(genericRepositoryInterfaceType, out object value))
            {
                return (IGenericRepository<TEntity>)value;
            }

            IGenericRepository<TEntity> repository = new GenericRepository<TEntity>(_context);
            _repositories[genericRepositoryInterfaceType] = repository;
            return repository;
        }
    }
}
