﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using NetcoreBase.Shared.Filters;
using NetcoreBase.Shared.Models;
using Newtonsoft.Json;
using System.IO.Compression;

namespace NetcoreBase.Shared.Extensions
{
    public static class StartupExtensions
    {
        public static void AddJsonDefaultSetting(this IServiceCollection services)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            };
        }

        public static void AddCustomControllers(this IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(GlobalExceptionFilters));
            })
            .ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var rs = context.ModelState
                        .Where(modelError => modelError.Value.Errors.Count > 0)
                        .Select(modelError => new
                        {
                            ErrorField = modelError.Key,
                            ErrorDescription = modelError.Value.Errors.FirstOrDefault()?.ErrorMessage
                        }).ToList();
                    LoggerExtensions.Info(rs);
                    return new BadRequestObjectResult(new BaseRespond<dynamic>
                    {
                        Status = StatusCodes.Status400BadRequest,
                        Message = "Bad request",
                        Data = rs
                    });
                };
            });
        }

        public static void AddMapper<T, V>(this IServiceCollection services) where T : Profile, new() where V : Profile, new()
        {
            var mapperConfig = new MapperConfiguration(config =>
            {
                config.AddProfile<T>();
                config.AddProfile<V>();
            });

            IMapper mapper = mapperConfig.CreateMapper();
            mapperConfig.AssertConfigurationIsValid();
            MappingExtensions.Init(mapper);
        }

        public static void AddMapper<T>(this IServiceCollection services) where T : Profile, new()
        {
            var mapperConfig = new MapperConfiguration(config =>
            {
                config.AddProfile<T>();
            });

            IMapper mapper = mapperConfig.CreateMapper();
            mapperConfig.AssertConfigurationIsValid();
            MappingExtensions.Init(mapper);
        }

        public static void AddCompressionResponse(this IServiceCollection services)
        {
            services.Configure<BrotliCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.SmallestSize;
            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.SmallestSize;
            });

            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.Providers.Add<BrotliCompressionProvider>();
                options.Providers.Add<GzipCompressionProvider>();
            });
        }
    }
}
