﻿using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace NetcoreBase.Shared.Extensions
{
    public static class MappingExtensions
    {
        private static IMapper _mapper;

        public static void Init(IMapper mapper)
        {
            _mapper = mapper;
        }

        public static TDto ToDtoWithContext<TDto>(this object source)
        {
            return _mapper.Map<TDto>(source, opt => opt.Items[source.GetType().Name] = source);
        }

        public static TEntity ToEntity<TEntity>(this object source)
        {
            return (TEntity)_mapper.Map(source, source.GetType(), typeof(TEntity));
        }

        public static TDto ToDto<TDto>(this object source)
        {
            return (TDto)_mapper.Map(source, source.GetType(), typeof(TDto));
        }

        public static TModel ToModel<TModel>(this object source)
        {
            return (TModel)_mapper.Map(source, source.GetType(), typeof(TModel));
        }

        public static TDest MapTo<TDest>(this object source)
        {
            if (source == null)
                return default;

            return (TDest)_mapper.Map(source, source.GetType(), typeof(TDest));
        }

        public static TDest MapTo<TDest>(this object obj1, TDest obj2)
        {
            if (obj1 == null)
                return default;

            return _mapper.Map(obj1, obj2);
        }

        public static IQueryable<TDest> ProjectTo<TDest>(this IQueryable source)
        {
            if (source == null)
                return default;

            return source.ProjectTo<TDest>(_mapper.ConfigurationProvider);
        }
    }
}
