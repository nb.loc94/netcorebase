﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NetcoreBase.Shared.Models;
using System.Net;
using System.Security.Authentication;

namespace NetcoreBase.Shared.Filters
{
    public sealed class GlobalExceptionFilters : ExceptionFilterAttribute
    {
        private readonly ILogger _logger;

        public GlobalExceptionFilters(ILogger<GlobalExceptionFilters> logger)
        {
            _logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            if (!context.ExceptionHandled)
            {
                var exception = context.Exception;
                switch (exception)
                {
                    case UnauthorizedAccessException or AuthenticationException:
                        _logger.LogDebug($"GlobalExceptionFilter: {context.ActionDescriptor.DisplayName}. {exception.Message}");
                        context.Result = new ObjectResult(new BaseRespond<Exception> { Status = (int)HttpStatusCode.Unauthorized, Message = exception.Message })
                        {
                            StatusCode = StatusCodes.Status401Unauthorized
                        };
                        break;
                    case MessageException:
                        _logger.LogWarning($"GlobalExceptionFilter: {context.ActionDescriptor.DisplayName}. {exception.Message}");
                        context.Result = new ObjectResult(new BaseRespond<dynamic>(exception))
                        {
                            StatusCode = ((MessageException)exception).StatusCode ?? StatusCodes.Status400BadRequest
                        };
                        break;
                    default:
                        _logger.LogError($"GlobalExceptionFilter: Error in {context.ActionDescriptor.DisplayName}. {exception.Message}. Stack Trace: {exception.StackTrace}");
                        context.Result = new ObjectResult(new BaseRespond<Exception>(exception))
                        {
                            StatusCode = StatusCodes.Status500InternalServerError
                        };
                        break;
                }
            }
        }
    }
}
