﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetcoreBase.Shared.Models
{
    public sealed class MessageException : Exception
    {
        public int? StatusCode { get; set; }
        public object? Error { get; set; } = null;

        public MessageException()
        {
        }

        public MessageException(int? statusCode, string message, object error) : base(message)
        {
            StatusCode = statusCode;
            Error = error;
        }

        public MessageException(int? statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public MessageException(string message) : base(message)
        {
        }

        public MessageException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
