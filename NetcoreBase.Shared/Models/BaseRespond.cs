﻿using System.Net;

namespace NetcoreBase.Shared.Models
{
    public sealed class BaseRespond<T> where T : new()
    {
        public BaseRespond()
        {
        }

        public BaseRespond(Exception ex)
        {
            if (ex is MessageException exception)
            {
                if (exception.StatusCode.HasValue)
                {
                    Status = exception.StatusCode.GetValueOrDefault();
                    Message = exception.Message;
                    Data = (T?)exception.Error;
                }
                else
                {
                    var rs = ex.Message.Split("|");
                    if (rs.Length > 1)
                    {
                        Status = int.Parse(rs[0]);
                        Message = rs[1];
                    }
                    else
                    {
                        Status = (int)HttpStatusCode.InternalServerError;
                        Message = ex.Message;
                    }
                }
            }
            else
            {
                Status = (int)HttpStatusCode.InternalServerError;
                Message = "Internal Error";
            }

        }

        public BaseRespond(MessageException ex)
        {
            if (ex.StatusCode.HasValue)
            {
                Status = (int)ex.StatusCode;
                Message = ex.Message;
            }
            else
            {
                var rs = ex.Message.Split("|");
                if (rs.Length > 1)
                {
                    Status = int.Parse(rs[0]);
                    Message = rs[1];
                }
                else
                {
                    Status = (int)HttpStatusCode.InternalServerError;
                    Message = ex.Message;
                }
            }
        }

        public int Status { get; set; } = -1;
        public string Message { get; set; }
        public T Data { get; set; }
    }

    public class BadRequestRespond
    {
        public string ErrorField { get; set; }
        public string ErrorDescription { get; set; }

        public BadRequestRespond()
        {
        }

        public BadRequestRespond(string errorField, string errorDescription)
        {
            ErrorField = errorField;
            ErrorDescription = errorDescription;
        }
    }
}
