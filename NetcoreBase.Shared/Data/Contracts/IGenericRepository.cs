﻿using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace NetcoreBase.Shared.Data.Contracts
{
    public interface IGenericRepository<T> where T : class
    {
        Task<List<T>> GetListAsync(Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null, bool isNoTracking = false, CancellationToken token = default);

        Task<List<V>> SelectListAsync<V>(Expression<Func<T, V>>? selector, Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
           Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null, CancellationToken token = default);

        Task<T> GetAsync(Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null, bool isNoTracking = false, CancellationToken token = default);

        Task<V> SelectAsync<V>(Expression<Func<T, V>>? selector, Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null, CancellationToken token = default);

        Task<T> GetById(object id);

        Task<T> AddAsync(T entity);

        Task<List<T>> AddRangeAsync(List<T> entity);

        Task<int> UpdateAsync(Expression<Func<SetPropertyCalls<T>, SetPropertyCalls<T>>> setPropertyCalls, Expression<Func<T, bool>>? filter = null);

        Task<T> UpdateAsync(T entity);

        Task<List<T>> UpdateRangeAsync(List<T> entity);

        Task<bool> DeleteAsync(T entity);
    }
}
