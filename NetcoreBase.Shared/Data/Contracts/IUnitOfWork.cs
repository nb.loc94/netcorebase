﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Data;

namespace NetcoreBase.Shared.Data.Contracts
{
    public interface IUnitOfWork
    {
        Task<IDbContextTransaction> BeginTransactionAsync();
        Task<IDbContextTransaction> BeginTransactionAsync(IsolationLevel isolationLevel);
        Task CommitAsync(IDbContextTransaction transaction);
        Task RollbackAsync(IDbContextTransaction transaction);
        Task<int> SaveChangesAsync();
    }
}
