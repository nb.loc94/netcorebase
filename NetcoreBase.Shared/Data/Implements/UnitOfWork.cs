﻿using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore;
using NetcoreBase.Shared.Data.Contracts;
using System.Data;

namespace NetcoreBase.Shared.Data.Implements
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<IDbContextTransaction> BeginTransactionAsync(IsolationLevel isolationLevel)
        {
            return _context.Database.BeginTransactionAsync(isolationLevel);
        }

        public Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return _context.Database.BeginTransactionAsync();
        }

        public Task CommitAsync(IDbContextTransaction transaction)
        {
            return transaction.CommitAsync();
        }

        public Task RollbackAsync(IDbContextTransaction transaction)
        {
            return transaction.RollbackAsync();
        }
    }
}
