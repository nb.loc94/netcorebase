﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using NetcoreBase.Shared.Data.Contracts;
using System.Linq.Expressions;

namespace NetcoreBase.Shared.Data.Implements
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly DbSet<T> _dbSet;

        public GenericRepository(DbContext dbContext)
        {
            _dbSet = dbContext.Set<T>();
        }

        public Task<List<T>> GetListAsync(Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null, bool isNoTracking = false, CancellationToken token = default)
        {
            IQueryable<T> query = _dbSet;
            if (include != null)
                query = include(query);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            if (isNoTracking)
                query = query.AsNoTrackingWithIdentityResolution();
            return query.ToListAsync(token);
        }

        public Task<List<V>> SelectListAsync<V>(Expression<Func<T, V>> selector, Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
           Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null, CancellationToken token = default)
        {
            IQueryable<T> query = _dbSet;
            if (include != null)
                query = include(query);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query.AsNoTrackingWithIdentityResolution().Select(selector).ToListAsync(token);
        }

        public Task<T> GetAsync(Expression<Func<T, bool>>? filter = null, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null, bool isNoTracking = false, CancellationToken token = default)
        {
            IQueryable<T> query = _dbSet;
            if (include != null)
                query = include(query);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            if (isNoTracking)
                query = query.AsNoTrackingWithIdentityResolution();

            return query.FirstOrDefaultAsync(token);
        }

        public Task<V?> SelectAsync<V>(Expression<Func<T, V>>? selector, Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null, CancellationToken token = default)
        {
            IQueryable<T> query = _dbSet;
            if (include != null)
                query = include(query);

            if (filter != null)
                query = query.Where(filter);

            return query.AsNoTrackingWithIdentityResolution().Select(selector).FirstOrDefaultAsync(token); ;
        }

        public async Task<T> GetById(object id)
        {
            return await _dbSet.FindAsync(id);
        }

        public Task<bool> Exist(Expression<Func<T, bool>>? filter = null, CancellationToken token = default)
        {
            return _dbSet.AnyAsync(filter, cancellationToken: token);
        }

        public Task<int> Max(Expression<Func<T, int>>? filter = null, CancellationToken token = default)
        {
            return _dbSet.MaxAsync(filter, cancellationToken: token);
        }

        public async Task<T> AddAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
            return entity;
        }

        public async Task<List<T>> AddRangeAsync(List<T> entity)
        {
            await _dbSet.AddRangeAsync(entity);
            return entity;
        }

        public Task<T> UpdateAsync(T entity)
        {
            _dbSet.Update(entity);
            return Task.FromResult(entity);
        }

        public Task<List<T>> UpdateRangeAsync(List<T> entity)
        {
            _dbSet.UpdateRange(entity);
            return Task.FromResult(entity);
        }

        public Task<int> UpdateAsync(Expression<Func<SetPropertyCalls<T>, SetPropertyCalls<T>>> setPropertyCalls, Expression<Func<T, bool>>? filter = null)
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            return query.ExecuteUpdateAsync(setPropertyCalls);
        }

        public Task<bool> DeleteAsync(T entity)
        {
            _dbSet.Remove(entity);
            return Task.FromResult(true);
        }

        public Task<bool> DeleteRangeAsync(List<T> entity)
        {
            _dbSet.RemoveRange(entity);
            return Task.FromResult(true);
        }
    }
}
